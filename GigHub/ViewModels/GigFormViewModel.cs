﻿using GigHub.Models;
using GigHub.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace GigHub.ViewModels
{
    public class GigFormViewModel
    {
        public string Venue { get; set; }

        public int Id { get; set; }

        [Required]
        [FutureDate]
        public string Date { get; set; }

        [Required]
        [ValidTime]
        public string Time { get; set; }

        public byte Genre { get; set; }

        public IEnumerable<Genre> Genres { get; set; }

        public string Heading { get; set; }

        /* folosim proprietatea asta in viewModel pt a putea reutiliza acelasi form atat pt create cat si pt edit 
         * Ca sa reusesti smechera ai nevoie sa definesti si proprietatea ID, astfel stii cand e create si cand e update
         * Deci de acum apare si Id-ul ca hidden element in form
         */
        public string Action
        {
            get
            {
                Expression<Func<GigsController, ActionResult>> update = (c => c.Update(this));
                Expression<Func<GigsController, ActionResult>> create = (c => c.Create(this));

                var action = (Id != 0) ? update : create;
                return (action.Body as MethodCallExpression).Method.Name;
            }
        }

        /* asta e sursa de date pt lista de Genres din formularul de adaugare */
        public System.DateTime GetDateTime()
        {
            return DateTime.Parse(string.Format("{0} {1}", Date, Time));
        }
    }
}