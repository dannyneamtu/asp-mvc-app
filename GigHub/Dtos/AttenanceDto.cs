﻿
namespace GigHub.Dtos
{
    /* dto = data transfer objects
     it's a arhitectural pattern to send data accross proccesses: 
     frontend-backend
     */
    public class AttendanceDto
    {
        public int GigId { get; set; }
    }
}