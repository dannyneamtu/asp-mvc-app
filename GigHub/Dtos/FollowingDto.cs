﻿
namespace GigHub.Dtos
{
    /* dto = data transfer objects
     it's a arhitectural pattern to send data accross proccesses: 
     frontend-backend
     */
    public class FollowingDto
    {
        public string FolloweeId { get; set; }
    }
}