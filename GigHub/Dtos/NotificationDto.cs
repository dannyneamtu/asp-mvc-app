﻿using GigHub.Models;
using System;

namespace GigHub.Dtos
{
    public class NotificationDto
    {
        /* this class sets the properties that we expose through our API */
        public DateTime DateTime { get; private set; }
        public NotificationType Type { get; private set; }
        /* ca only be set using factory method( see this file, the 3 methods 
         notification = imutable object */
        public DateTime? OriginalDateTime { get; protected set; }
        public string OriginalVenue { get; protected set; }
        public GigDto Gig { get; protected set; }
    }
}