﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GigHub.Models
{
    public class UserNotification
    {
        [Key]
        [Column(Order = 1)]
        public string UserId { get; private set; }

        [Key]
        [Column(Order = 2)]
        public int NotificationId { get; private set; }

        public bool IsRead { get; set; }

        // campurile urmatoare sunt navigational properties - necesare sa navighezi la inregistrarea corespunzatoare
        // metodele setters se seteaza private pt a nu permite schimbarea unilaterala
        // UserNotification e o combinatie de un User si o notificare
        public ApplicationUser User { get; private set; }

        public Notification Notification { get; private set; }

        /* avem nevoie de acest default constructor pt a fi folosit de EntityFramework, 
         * deoarece am definit un constructor custom, mai jos*/
        protected UserNotification()
        {
        }

        public UserNotification(ApplicationUser user, Notification notification)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (notification == null)
                throw new ArgumentNullException("notification");

            User = user;
            Notification = notification;
        }

        public void Read()
        {
            IsRead = true;
        }
    }
}